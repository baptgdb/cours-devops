use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use gethostname::gethostname; 

fn main() {
    let listener = TcpListener::bind("0.0.0.0:8080").unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream);
    }
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();
    let request = String::from_utf8_lossy(&buffer[..]);

    // Check if the request contains "GET /ping"
    let response = if request.contains("GET /ping") {
        // I want to display the request headers in JSON format within text/plain
        let hostname = gethostname().to_string_lossy().to_string(); // Create a named variable to store the hostname
        let log = format!("{}: {}", hostname, request);
        println!("{}", log);


        let headers = request.lines().collect::<Vec<&str>>();
        let mut json = String::from("{\n");
        for header in headers {
            if let Some(pos) = header.find(": ") {
                let (key, value) = header.split_at(pos);
                let value = &value[2..]; // Removing ": " from the value
                if key != "GET /ping HTTP/1.1" {
                    json.push_str(&format!("  \"{}\": \"{}\",\n", key, value));
                }
            }
        }
        json.push_str("}\n");

        format!("HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n{}\r\n", json)
    } else {
        "HTTP/1.1 404 NOT FOUND\r\nContent-Type: text/plain\r\n\r\n".to_string()
    };

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}
