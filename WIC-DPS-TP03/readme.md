# TP03 - WIC-DPS

## docker compose 

### custom nginx.conf

![nginx](https://symbiotics.co.za/wp-content/uploads/2016/01/dicappriorevers-proxy.jpeg)

- nginx.conf
```conf
worker_processes 1;

events {
    worker_connections 1024;
}

http {
    log_format main '$host $remote_user - $time_local "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log /var/log/nginx/access.log main;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    server {
        listen 80;

        location / {
            proxy_pass http://my-app:8080;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }

        access_log /var/log/nginx/my-app-access.log main;
    }
}
```

### docker-compose.yml

![Docker](https://img-9gag-fun.9cache.com/photo/ap20YbB_460s.jpg)

- docker-compose.yml
```yml
version: '3.7'

services:
  reverse-proxy:
    image: nginx
    ports:
      - "8080:80"
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    depends_on:
      - my-app
    networks:
      - my-network

  my-app:
    build:
      context: .
      dockerfile: Dockerfile
    expose:
      - "8081"
    deploy:
      replicas: 4
    networks:
      - my-network

networks:
  my-network:
    driver: bridge
```

### main.rs

- main.rs
```rust
use gethostname::gethostname; 
  let hostname = gethostname().to_string_lossy().to_string(); // Create a named variable to store the hostname
  let log = format!("{}: {}", hostname, request);
  println!("{}", log);
```

### docker-compose

- docker-compose build

- docker-compose up -d
```bash
... 
Status: Downloaded newer image for nginx:latest
Creating wic-dps-tp03_myApp_1 ... done
Creating wic-dps-tp03_myApp_2 ... done
Creating wic-dps-tp03_myApp_3 ... done
Creating wic-dps-tp03_myApp_4 ... done
Creating wic-dps-tp03_reverse-proxy_1 ... done
```

- docker-compose ps
```bash
           Name                        Command             State             Ports           
---------------------------------------------------------------------------------------------
wic-dps-tp03_my-app_1         ./my-app                     Up      8080/tcp, 8081/tcp        
wic-dps-tp03_my-app_2         ./my-app                     Up      8080/tcp, 8081/tcp        
wic-dps-tp03_my-app_3         ./my-app                     Up      8080/tcp, 8081/tcp        
wic-dps-tp03_my-app_4         ./my-app                     Up      8080/tcp, 8081/tcp        
wic-dps-tp03_reverse-         /docker-entrypoint.sh ngin   Up      0.0.0.0:8080-             
proxy_1                       ...                                  >80/tcp,:::8080->80/tcp   
```

### test

- docker-compose logs -f
```bash
Attaching to wic-dps-tp03_my-app_1, wic-dps-tp03_my-app_4, wic-dps-tp03_my-app_3, wic-dps-tp03_my-app_2, wic-dps-tp03_reverse-proxy_1
reverse-proxy_1  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
reverse-proxy_1  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
reverse-proxy_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
reverse-proxy_1  | 10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
reverse-proxy_1  | 10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
reverse-proxy_1  | /docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
reverse-proxy_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
reverse-proxy_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
reverse-proxy_1  | /docker-entrypoint.sh: Configuration complete; ready for start up
my-app_2         | 8eae9b0ebe0e: GET /ping HTTP/1.0
my-app_2         | Host: localhost
my-app_2         | X-Real-IP: 172.18.0.1
my-app_2         | X-Forwarded-For: 172.18.0.1
my-app_2         | X-Forwarded-Proto: http
my-app_2         | Connection: close
my-app_2         | User-Agent: curl/8.0.1
my-app_2         | Accept: */*
my-app_2         | 
my-app_2         | 
my-app_4         | d21dfe468025: GET /ping HTTP/1.0
my-app_4         | Host: localhost
my-app_4         | X-Real-IP: 172.18.0.1
my-app_4         | X-Forwarded-For: 172.18.0.1
my-app_4         | X-Forwarded-Proto: http
my-app_4         | Connection: close
my-app_4         | User-Agent: curl/8.0.1
my-app_4         | Accept: */*
my-app_4         | 
my-app_4         | 
my-app_1         | d7841937f048: GET /ping HTTP/1.0
my-app_1         | Host: localhost
my-app_1         | X-Real-IP: 172.18.0.1
my-app_1         | X-Forwarded-For: 172.18.0.1
my-app_1         | X-Forwarded-Proto: http
my-app_1         | Connection: close
my-app_1         | User-Agent: curl/8.0.1
my-app_1         | Accept: */*
my-app_1         | 
my-app_1         | 
my-app_3         | 3078f327c1b7: GET /ping HTTP/1.0
my-app_3         | Host: localhost
my-app_3         | X-Real-IP: 172.18.0.1
my-app_3         | X-Forwarded-For: 172.18.0.1
my-app_3         | X-Forwarded-Proto: http
my-app_3         | Connection: close
my-app_3         | User-Agent: curl/8.0.1
my-app_3         | Accept: */*
my-app_3         | 
my-app_3         | 
```

ça fonctionne en cycle 2-4-1-3


- curl localhost:8080 -v
```bash
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 404 NOT FOUND
< Server: nginx/1.25.3
< Date: Mon, 30 Oct 2023 14:04:51 GMT
< Content-Type: text/plain
< Transfer-Encoding: chunked
< Connection: keep-alive
< 
* Connection #0 to host localhost left intact
```

- curl localhost:8080/ping -v
```bash
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /ping HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Server: nginx/1.25.3
< Date: Mon, 30 Oct 2023 14:04:56 GMT
< Content-Type: text/plain
< Transfer-Encoding: chunked
< Connection: keep-alive
< 
{
  "Host": "localhost",
  "X-Real-IP": "172.18.0.1",
  "X-Forwarded-For": "172.18.0.1",
  "X-Forwarded-Proto": "http",
  "Connection": "close",
  "User-Agent": "curl/8.0.1",
  "Accept": "*/*",
}
```




![the end](https://media.makeameme.org/created/docker-i-see.jpg)

