# WIC-DPS-TP01

Ce projet est un serveur HTTP simple écrit en Rust. Il écoute les connexions entrantes sur localhost au port 8080 et renvoie une réponse HTTP en fonction de la requête reçue.
Prérequis

Assurez-vous d'avoir installé Rust sur votre système. Si ce n'est pas le cas, vous pouvez le télécharger et l'installer à partir du site officiel de Rust : https://www.rust-lang.org/

## Comment ça marche :

Ce serveur utilise la bibliothèque standard de Rust pour gérer les connexions TCP. Lorsqu'une connexion est établie, le serveur lit la requête HTTP entrante et vérifie si elle contient le chemin "/ping". Si c'est le cas, le serveur renvoie une réponse "200 OK" avec le message "Hello, world!". Sinon, le serveur renvoie une réponse "404 NOT FOUND".

## Comment tester :

Pour tester le serveur, exécutez les commandes suivantes :

- cargo new hello-rust

- cd hello-rust/src

puis copier le code dans main.rs

- rustc main.rs

- ./main

- curl http://localhost:8080/ping
```bash
{
  "Host": "localhost:8080",
  "User-Agent": "curl/8.0.1",
  "Accept": "*/*",
}
```

- curl http://localhost:8080/ping -v
```bash
*   Trying 127.0.0.1:8080...
* connect to 127.0.0.1 port 8080 failed: Connection refused
*   Trying [::1]:8080...
* Connected to localhost (::1) port 8080 (#0)
> GET /ping HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Content-Type: text/plain
* no chunk, no close, no size. Assume close to signal end
< 
{
  "Host": "localhost:8080",
  "User-Agent": "curl/8.0.1",
  "Accept": "*/*",
}

* Closing connection 0
```

- curl http://localhost:8080/ping 
```bash

```

- curl http://localhost:8080 -v
```bash
*   Trying 127.0.0.1:8080...
* connect to 127.0.0.1 port 8080 failed: Connection refused
*   Trying [::1]:8080...
* Connected to localhost (::1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 404 NOT FOUND
< Content-Type: text/plain
* no chunk, no close, no size. Assume close to signal end
< 
* Closing connection 0
```
