use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};

fn main() {
    let listener = TcpListener::bind("localhost:8080").unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream);
    }
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();
    let request = String::from_utf8_lossy(&buffer[..]);

    // Check if the request contains "GET /ping"
    let response = if request.contains("GET /ping") {
        // je veux afficher les headers de la requete au format json dans le  text/plain

        let headers = request.split("\r\n").collect::<Vec<&str>>();
        let mut json = String::from("{\n");
        for header in headers {
            let mut header = header.split(": ");
            let key = header.next().unwrap();
            let value = header.next().unwrap_or("");
            if key != "" && value != "" && key != "GET /ping HTTP/1.1"{
                json.push_str(&format!("  \"{}\": \"{}\",\n", key, value));
            }
            
        }
        json.push_str("}\n");

        format!("HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n{}\r\n", json)
    } else {
        "HTTP/1.1 404 NOT FOUND\r\nContent-Type: text/plain\r\n\r\n".to_string()
    };

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}

