# WIC-DPS-TP04

## TP 4: Kubernetes

![image](meme1.png)

### Prérequis

- curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm

- sudo rpm -Uvh minikube-latest.x86_64.rpm

- minikube start

![image](meme2.png)

### Étape 1
    
- fichier YAML pour le Pod (pod.yaml) avec le contenu suivant :
 
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-app-pod
spec:
  containers:
  - name: my-app-container
    image: registry.cluster.wik.cloud/public/echo
    ports:
    - containerPort: 8080

```

- le fichier YAML pour créer le Pod :
    
```bash
kubectl apply -f pod.yaml
```

- un port-forwarding pour tester le Pod en local :

```bash
kubectl port-forward my-app-pod 8080:8080
```

- Testez le Pod en local :

```bash
curl localhost:8080/ping
{"host":"localhost:8080","user-agent":"curl/8.0.1","accept":"*/*"}(base) 
```

### Étape 2

- fichier YAML pour le ReplicaSet pod.yaml avec le contenu suivant :

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: my-app-replicaset
spec:
  replicas: 4
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: my-app-container
        image: registry.cluster.wik.cloud/public/echo
        ports:
        - containerPort: 8080
```

- le fichier YAML pour créer le ReplicaSet :

```bash
kubectl apply -f pod.yaml
```

- Testez le ReplicaSet en local :

```bash
kubectl get pods 
NAME                      READY   STATUS    RESTARTS   AGE
my-app-pod                1/1     Running   0          15m
my-app-replicaset-2qdjc   1/1     Running   0          9m44s
my-app-replicaset-6j7cl   1/1     Running   0          9m44s
my-app-replicaset-fskr6   1/1     Running   0          9m44s
my-app-replicaset-tscdc   1/1     Running   0          9m44s
```

### Étape 3

- fichier YAML pour le Service (pod.yaml) avec le contenu suivant :

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: my-app-replicaset
spec:
  replicas: 4
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
  spec:
    selector:
      app: my-app
    ports:
      - protocol: TCP
        port: 8080
        targetPort: 8080
    type: ClusterIP
```

- Testez le Service en local :
```bash
curl localhost:8080/ping
{
  "Host": "localhost",
  "X-Real-IP": "172.18.0.1",
  "X-Forwarded-For": "172.18.0.1",
  "X-Forwarded-Proto": "http",
  "Connection": "close",
  "User-Agent": "curl/8.0.1",
  "Accept": "*/*",
}
```

- le fichier YAML pour créer le Service :

```bash
kubectl apply -f pod.yaml
```

- Activez le plugin Ingress NGINX sur Minikube :

```bash
minikube addons enable ingress
```

- fichier YAML pour l'Ingress (Ingress.yaml) avec le contenu suivant :

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-app-ingress
spec:
  rules:
  - host: mon-domaine.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: my-app-service
            port:
              number: 8080
```

- le fichier YAML pour créer l'Ingress :

```bash
kubectl apply -f Ingress.yaml
```

### Étape 4

- nom de domaine >> /etc/hosts :

```bash
sudo echo "127.0.0.1 mon-domaine.com" >> /etc/hosts
```

- Testez l'Ingress en local :

```bash
 curl localhost:8080/ping
{
  "Host": "localhost",
  "X-Real-IP": "172.18.0.1",
  "X-Forwarded-For": "172.18.0.1",
  "X-Forwarded-Proto": "http",
  "Connection": "close",
  "User-Agent": "curl/8.0.1",
  "Accept": "*/*",
}


```

![image](Screenshot 2023-11-18 at 19-44-25 Screenshot.png)