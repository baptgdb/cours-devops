# TP02 - WIC-DPS

## docker

pour ce TP dans le ficker main.rs a la ligne 5 il faut changer la ligne :
```bash
let listener = TcpListener::bind("localhost:8080").unwrap();
```
en :
```bash
let listener = TcpListener::bind("0.0.0.0:8080").unwrap();
```

### setup

- install docker
    - [docker install](https://docs.docker.com/engine/install/ubuntu/)


### build

- build docker image
```bash
sudo docker build -t rust:test Dockerfile
ou 
sudo docker build -t rust:test Dockerfile
```
- sudo docker build -t rust:test Dockerfile
  - si il n'y a qu'un dockerfile dans le dossier courant, il n'est pas nécessaire de préciser le nom du dockerfile

### run

- run docker image

```bash
sudo docker run -p 8080:8080 -it rust:test
```

### I.

#### dockerfile

- dockerfile
```bash
# Use a specific version of the official Rust base image
# Use the latest version of the Rust base image
FROM rust:latest

# Create a user rust-app
USER rust-app

# Set the working directory in the container to /usr/src/my-app
WORKDIR /my-app

# Open port 8080
EXPOSE 8080

# Copy the Rust project files to the working directory
COPY . .

# Build the Rust app
RUN cargo build --release

# Run the Rust app
CMD ./target/release/my-app
```

#### test

- curl http://localhost:8080/ping -v
```bash
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /ping HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Content-Type: text/plain
* no chunk, no close, no size. Assume close to signal end
< 
{
  "Host": "localhost:8080",
  "User-Agent": "curl/8.0.1",
  "Accept": "*/*",
}
* Closing connection 0
```

- curl http://localhost:8080/ -v
```bash
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 404 NOT FOUND
< Content-Type: text/plain
* no chunk, no close, no size. Assume close to signal end
< 
* Closing connection 0
```

ça fonctionne !!!


### II.

#### dockerfile

- dockerfile
```bash
# Use the latest version of the Rust base image for building
FROM rust:latest AS builder

# Install the x86_64-unknown-linux-musl target
RUN rustup target add x86_64-unknown-linux-musl

# Set the working directory in the builder container
WORKDIR /my-app

# Copy the Rust project files to the builder container
COPY . .

# Build the Rust project with a static binary
RUN cargo build --release --target x86_64-unknown-linux-musl

# Create a new stage with a minimal Alpine Linux base image
FROM alpine:latest as final

# Open port 8080
EXPOSE 8080

# Create app directory
RUN mkdir /app

# create toto as user and group notUser for /app
RUN addgroup -S notUser && adduser -S toto -G notUser

# change owner of /app to toto
RUN chown -R toto:notUser /app

# change the user to toto
USER toto

# Set the working directory in the final container
WORKDIR /app

# Copy the binary from the builder stage
COPY --from=builder /my-app/target/x86_64-unknown-linux-musl/release/my-app /app/my-app

# Run the binary
CMD ["./my-app"]

```

#### build and run

- build docker image
```bash
sudo docker build -t rust:test . -f Dockerfile2
```

- run docker image

```bash
sudo docker run -p 8080:8080 -it rust:test
```

#### test

- curl http://localhost:8080/ping -v
```bash
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /ping HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Content-Type: text/plain
* no chunk, no close, no size. Assume close to signal end
< 
{
  "Host": "localhost:8080",
  "User-Agent": "curl/8.0.1",
  "Accept": "*/*",
}

* Closing connection 0
```

- curl http://localhost:8080 -v
```bash
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 404 NOT FOUND
< Content-Type: text/plain
* no chunk, no close, no size. Assume close to signal end
< 
* Closing connection 0
```

#### docker scout

- docker scout cves test 
```bash


#### docker scout

- docker scout cves test 
```bash
# Install Docker Engine on Fedora 38
sudo dnf install docker-ce

# Clone the Docker Scout repository
git clone https://github.com/docker/scout.git

# Change to the Docker Scout directory
cd scout

# Build the Docker Scout image
docker build -t scout .

docker login

```

- docker scout cves

```bash
# Run the Docker Scout container
docker run -d -p 8080:8080 scout

- docker scout cves test /path/to/your/Dockerfile

sudo docker scout cves test /mnt/data/code/devops/cours-devops/WIC-DPS-TP02/Dockerfile

sudo docker scout cves test /mnt/data/code/devops/cours-devops/WIC-DPS-TP02/Dockerfile2
```